import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';
const firebaseConfig = {
  apiKey: "AIzaSyDQjDFo39S48ur1Q5FKk4TY8PJxIXi1Koc",
  authDomain: "cambio-horario-utpl.firebaseapp.com",
  projectId: "cambio-horario-utpl",
  storageBucket: "cambio-horario-utpl.appspot.com",
  messagingSenderId: "1046185163454",
  appId: "1:1046185163454:web:bf4ab71c3b7e4eb01afcef",
  measurementId: "G-5KB1TPHTNY",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();

export default db;
