/** @jsxRuntime classic */
/** @jsx jsx */
import { Typography } from "@mui/material";
import Container from "@mui/material/Container";
import { css, jsx } from "@emotion/react";
import logoUtpl from "../../static/img/logo_white_utpl.png";

const topHeader = css`
  font-weight: bolder;
  padding: 0.3rem 0;
`;

const bannerBlueUtpl = (theme) => css`
  background-color: ${theme.palette.primary.main};
  padding: 0.8rem 0;
`;

const sTitle = (theme) => css`
  padding: 0.8rem 0;
  border-bottom: 0.15rem solid ${theme.palette.secondary.light};
`;
const Title = (theme) => css`
  font-weight: bolder;
`;

export default function Header() {
  return (
    <header className="App-header">
      <Container fixed>
        <Typography
          css={topHeader}
          variant="subtitle2"
          color="primary"
          component="div"
        >
          Universidad Técnica Particular de Loja
        </Typography>
      </Container>
      <section css={bannerBlueUtpl}>
        <Container fixed>
          <img src={logoUtpl} alt="logo utpl" />
        </Container>
      </section>
      <section css={sTitle}>
        <Container fixed>
          <Typography css={Title} variant="h4" color="primary">
            Cambios de Horarios
          </Typography>
          <Typography css={Title} variant="body1" color="primary">
            Centro de Evaluaciones
          </Typography>
        </Container>
      </section>
    </header>
  );
}
