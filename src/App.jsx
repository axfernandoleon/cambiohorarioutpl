/** @jsxRuntime classic */
/** @jsx jsx */
import React from "react";
import { useState } from "react";
import "./App.css";
import Header from "./components/header";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import { Typography } from "@mui/material";
import { Button } from "@mui/material";
import { TextField } from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import MuiAlert from "@mui/material/Alert";
import Divider from "@mui/material/Divider";
import Snackbar from "@mui/material/Snackbar";
import Slide from "@mui/material/Slide";
import Box from "@mui/material/Box";
import EventIcon from "@mui/icons-material/Event";
import { css, jsx } from "@emotion/react";
import db from "./firebaseConfig";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});
const theme = createTheme({
  palette: {
    primary: {
      // light: will be calculated from palette.primary.main,
      main: "#003F72",
      contrastText: "#ffff",
      // dark: will be calculated from palette.primary.main,
      // contrastText: will be calculated to contrast with palette.primary.main
    },
    secondary: {
      main: "#EAAB00",
      // dark: will be calculated from palette.secondary.main,
      contrastText: "#ffcc00",
      light: "#A3A3A3",
    },
  },
});

const buttonConsulta = css`
  border: 3px solid rgb(0 63 114 / 85%);
  :hover {
    border: 3px solid ${theme.palette.primary.main};
  }
`;

function App() {
  const initialStateValues = {
    cedula: "",
    data: undefined,
  };

  const [values, setValues] = useState(initialStateValues);
  const [open, setOpen] = React.useState(false);

  const vertical = "bottom";
  const horizontal = "center";

  const handleSubmit = (e) => {
    const { id, value } = e.target;
    setValues({ ...values, [id]: value });
  };

  const getCedula = (c) => {
    console.log("entro");
    if (isNaN(parseInt(c))) {
      return c.toUpperCase();
    } else {
      return parseInt(c);
    }
  };

  const submit = () => {
    if (values.cedula.length === 0) {
      setOpen(true);
    } else {
      db.collection("cambiosBM2")
        .where("cedula", "==", getCedula(values.cedula))
        .get()
        .then((querySnapshot) => {
          if (querySnapshot.docs.length === 0) {
            setOpen(true);
            setValues({ ...values, data: undefined });
          } else {
            querySnapshot.forEach((doc) => {
              setValues({ ...values, data: doc.data() });
            });
            //console.log(values);
          }
        })
        .catch((error) => {
          console.log("Error getting documents: ", error);
        });
    }
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  function TransitionUp(props) {
    return <Slide {...props} direction="up" />;
  }

  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <Header />
        <Container fixed>
          <Grid
            container
            rowSpacing={1}
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
          >
            <Grid item xs={12} md={6} mt={2}>
              <Grid container rowSpacing={6}>
                <Grid item xs={12}>
                  <Typography variant="h6" color="primary" fontWeight="bold">
                    Periodo
                  </Typography>
                  <Typography variant="h5" color="primary">
                    Octubre 2021/ Febrero 2022
                  </Typography>
                </Grid>
                <Grid item xs={10} md={9}>
                  <TextField
                    fullWidth
                    label="Cédula/Pasaporte"
                    id="cedula"
                    placeholder="11XXXXXXX7"
                    helperText="Por favor, ingresa tu identificación"
                    variant="standard"
                    size="medium"
                    color="secondary"
                    onChange={handleSubmit}
                    p={2}
                  />
                  <Snackbar
                    open={open}
                    autoHideDuration={6000}
                    onClose={handleClose}
                    TransitionComponent={TransitionUp}
                    anchorOrigin={{ vertical, horizontal }}
                  >
                    <Alert open={open} onClose={handleClose} severity="error">
                      No existe ningún registro del cambio
                    </Alert>
                  </Snackbar>
                </Grid>
                <Grid item xs={12}>
                  <Button
                    css={buttonConsulta}
                    variant="outlined"
                    color="primary"
                    size="large"
                    onClick={submit}
                  >
                    Consultar
                  </Button>
                </Grid>
              </Grid>
            </Grid>
            {values.data ? (
              <Grid item xs={12} md={6} mt={2}>
                <Typography variant="h5" color="primary" fontWeight="bold">
                  Cambios de Horarios
                </Typography>

                <Typography
                  variant="h6"
                  color="primary"
                  fontWeight="bold"
                  pt={2}
                  pb={1}
                >
                  {values.data.nombres.toUpperCase()}
                  <Typography variant="body1" color="primary">
                    {values.data.cedula}
                  </Typography>
                </Typography>

                <Card
                  sx={{
                    minWidth: 275,
                    borderLeft: 4,
                    borderColor: "secondary.main",
                  }}
                >
                  <CardContent>
                    <Typography
                      sx={{ mb: 1.5 }}
                      color="primary"
                      fontWeight="bold"
                      variant="body1"
                    >
                      Primer Cambio
                    </Typography>
                    {values.data["1er Cambio - Asignado UTPL - actual"] ? (
                      <Typography
                        sx={{ mb: 1.5, mt: 1.5 }}
                        variant="body1"
                        color="text.primary"
                      >
                        {values.data["1er Cambio - Asignado UTPL - actual"]}{" "}
                        <strong> ></strong>{" "}
                        {values.data["1er Cambio - Nuevo Horario"]}
                      </Typography>
                    ) : (
                      <Typography variant="body1" color="text.primary">
                        -
                      </Typography>
                    )}

                    <Divider light />
                    <Typography
                      sx={{ mb: 1.5, mt: 1.5 }}
                      color="primary"
                      fontWeight="bold"
                    >
                      Segundo Cambio
                    </Typography>
                    {values.data["2er Cambio - Asignado UTPL - actual"] ? (
                      <Typography variant="body1" color="text.primary">
                        {values.data["2er Cambio - Asignado UTPL - actual"]}{" "}
                        <strong> ></strong>{" "}
                        {values.data["2er Cambio - Nuevo Horario"]}
                      </Typography>
                    ) : (
                      <Typography variant="body1" color="text.primary">
                        -
                      </Typography>
                    )}
                  </CardContent>
                </Card>
              </Grid>
            ) : (
              <Grid item xs={12} md={6} mt={2}>
                <Typography variant="h5" color="primary" fontWeight="bold">
                  Cambios de Horarios
                </Typography>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                    height: "100%",
                  }}
                  mb={15}
                >
                  <EventIcon
                    style={{ color: "#a3a3a3" }}
                    sx={{ fontSize: 200 }}
                  />
                  <Typography
                    variant="body1"
                    color="secondary.light"
                    textAlign="center"
                  >
                    Recuerda que la información presentada es única, personal e
                    intransferible
                  </Typography>
                </Box>
              </Grid>
            )}
          </Grid>
        </Container>
      </div>
    </ThemeProvider>
  );
}

export default App;
